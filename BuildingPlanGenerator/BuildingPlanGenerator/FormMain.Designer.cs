﻿namespace BuildingPlanGenerator
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBoxMap = new System.Windows.Forms.PictureBox();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxBuildingWidth = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxBuildingLength = new System.Windows.Forms.TextBox();
            this.textBoxRoomNum = new System.Windows.Forms.TextBox();
            this.textBoxFloorNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSchemeId = new System.Windows.Forms.TextBox();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControlPlans = new System.Windows.Forms.TabControl();
            this.linkLabelOpenFile = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMap)).BeginInit();
            this.statusStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(350, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 33;
            // 
            // pictureBoxMap
            // 
            this.pictureBoxMap.BackColor = System.Drawing.Color.White;
            this.pictureBoxMap.Location = new System.Drawing.Point(545, 153);
            this.pictureBoxMap.Name = "pictureBoxMap";
            this.pictureBoxMap.Size = new System.Drawing.Size(690, 490);
            this.pictureBoxMap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxMap.TabIndex = 29;
            this.pictureBoxMap.TabStop = false;
            this.pictureBoxMap.UseWaitCursor = true;
            this.pictureBoxMap.Visible = false;
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(324, 6);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(139, 79);
            this.buttonGenerate.TabIndex = 28;
            this.buttonGenerate.Text = "Генерировать планы";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Ширина здания (м)";
            // 
            // textBoxBuildingWidth
            // 
            this.textBoxBuildingWidth.Location = new System.Drawing.Point(121, 65);
            this.textBoxBuildingWidth.Name = "textBoxBuildingWidth";
            this.textBoxBuildingWidth.Size = new System.Drawing.Size(40, 20);
            this.textBoxBuildingWidth.TabIndex = 26;
            this.textBoxBuildingWidth.Text = "20";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Длина здания (м)";
            // 
            // textBoxBuildingLength
            // 
            this.textBoxBuildingLength.Location = new System.Drawing.Point(121, 40);
            this.textBoxBuildingLength.Name = "textBoxBuildingLength";
            this.textBoxBuildingLength.Size = new System.Drawing.Size(40, 20);
            this.textBoxBuildingLength.TabIndex = 24;
            this.textBoxBuildingLength.Text = "30";
            // 
            // textBoxRoomNum
            // 
            this.textBoxRoomNum.Enabled = false;
            this.textBoxRoomNum.Location = new System.Drawing.Point(278, 65);
            this.textBoxRoomNum.Name = "textBoxRoomNum";
            this.textBoxRoomNum.Size = new System.Drawing.Size(40, 20);
            this.textBoxRoomNum.TabIndex = 22;
            // 
            // textBoxFloorNum
            // 
            this.textBoxFloorNum.Enabled = false;
            this.textBoxFloorNum.Location = new System.Drawing.Point(278, 39);
            this.textBoxFloorNum.Name = "textBoxFloorNum";
            this.textBoxFloorNum.Size = new System.Drawing.Size(40, 20);
            this.textBoxFloorNum.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Id";
            // 
            // textBoxSchemeId
            // 
            this.textBoxSchemeId.Location = new System.Drawing.Point(41, 8);
            this.textBoxSchemeId.Name = "textBoxSchemeId";
            this.textBoxSchemeId.Size = new System.Drawing.Size(277, 20);
            this.textBoxSchemeId.TabIndex = 18;
            // 
            // statusStripMain
            // 
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStripMain.Location = new System.Drawing.Point(0, 704);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Size = new System.Drawing.Size(1247, 22);
            this.statusStripMain.TabIndex = 36;
            this.statusStripMain.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            this.toolStripStatusLabel1.TextChanged += new System.EventHandler(this.toolStripStatusLabel1_TextChanged);
            // 
            // tabControlPlans
            // 
            this.tabControlPlans.Location = new System.Drawing.Point(22, 135);
            this.tabControlPlans.Name = "tabControlPlans";
            this.tabControlPlans.SelectedIndex = 0;
            this.tabControlPlans.Size = new System.Drawing.Size(473, 490);
            this.tabControlPlans.TabIndex = 38;
            // 
            // linkLabelOpenFile
            // 
            this.linkLabelOpenFile.AutoSize = true;
            this.linkLabelOpenFile.Location = new System.Drawing.Point(19, 109);
            this.linkLabelOpenFile.Name = "linkLabelOpenFile";
            this.linkLabelOpenFile.Size = new System.Drawing.Size(78, 13);
            this.linkLabelOpenFile.TabIndex = 39;
            this.linkLabelOpenFile.TabStop = true;
            this.linkLabelOpenFile.Text = "открыть файл";
            this.linkLabelOpenFile.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelOpenFile_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(176, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Кол-во комнат";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(176, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Кол-во этажей";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1247, 726);
            this.Controls.Add(this.linkLabelOpenFile);
            this.Controls.Add(this.tabControlPlans);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBoxMap);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxBuildingWidth);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxBuildingLength);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxRoomNum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxFloorNum);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxSchemeId);
            this.MinimumSize = new System.Drawing.Size(600, 600);
            this.Name = "FormMain";
            this.Text = "Buiding Plan Generator";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMap)).EndInit();
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBoxMap;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxBuildingWidth;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxBuildingLength;
        private System.Windows.Forms.TextBox textBoxRoomNum;
        private System.Windows.Forms.TextBox textBoxFloorNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSchemeId;
        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TabControl tabControlPlans;
        private System.Windows.Forms.LinkLabel linkLabelOpenFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}

