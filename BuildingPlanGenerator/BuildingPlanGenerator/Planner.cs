﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace BuildingPlanGenerator
{
    class Planner
    {
        public List<FloorMap> floors;
        public static string PlansFileName = System.IO.Directory.GetCurrentDirectory() + "\\plans.xml";
        public static string PlansFolder = System.IO.Directory.GetCurrentDirectory() + "\\plans\\";

        private SchemaAttributes schemaAttributes;
        public Planner(SchemaAttributes _schemaAttributes)
        {
            this.floors = new List<FloorMap>();
            for (int i = 0; i < _schemaAttributes.FloorNum; i++)
            {
                floors.Add(new FloorMap(_schemaAttributes));
            }

            this.schemaAttributes = _schemaAttributes;
        }

        public Planner()
        {
            this.floors = new List<FloorMap>();
        }

        public Schema Plan(ref  PictureBox _pictureBox, ref ToolStripStatusLabel _statusLabel, double _scale)
        {

            int retryCount = 0;
            int retryLimit = 10;
            int squareLimit = 5;
            int narrowPartLimit = 60;

            bool forcedRetry = false;

            Schema schema = new Schema();


            for (int i = 0; i < this.floors.Count; i++)
            {
                retryCount = 0;
                do
                {
                    forcedRetry = false;
                    
                    this.floors[i] = new FloorMap(this.schemaAttributes);
                    _statusLabel.Text = "Этаж " + i.ToString() + ": генерирование стен здания";
                    this.floors[i].MakeBuildingWalls(ref _pictureBox);
                    _statusLabel.Text = "Этаж " + i.ToString() + ": генерирование корридора";
                    this.floors[i].MakeCorridors(ref _pictureBox);
                    // Проверка: Оставшаяся свободная площадь больше чем ([Площадь здания]*0.05*[Кол-во комнат]) 

                    if (floors[i].GetEmptySquare() > this.schemaAttributes.BuildingLength * this.schemaAttributes.BuildingWidth*0.05*this.schemaAttributes.RoomNum)
                    {
                        _statusLabel.Text = "Этаж " + i.ToString() + ": генерирование комнат";
                        this.floors[i].MakeRooms(ref _pictureBox);
                        if (this.floors[i].IsAllRoomsHasMinSquare(squareLimit))
                        {
                            _statusLabel.Text = "Этаж " + i.ToString() + ": генерирование стен";
                            this.floors[i].MakeRoomWalls(ref _pictureBox);
                            _statusLabel.Text = "Этаж " + i.ToString() + ": генерирование дверей";
                            this.floors[i].MakeDoors(ref _pictureBox);
                            _statusLabel.Text = "";
                            retryCount++; 
                        }
                        else
                        {
                            forcedRetry = true; 
                        }
                       
                    }
                    else
                    {
                        forcedRetry = true;
                    }



                } while (((!this.floors[i].IsAllRoomsHasMinSquare(squareLimit) || !this.floors[i].IsAllRoomsHaveNotNarrowPart(narrowPartLimit)) && retryCount < retryLimit) || (forcedRetry));



                schema.Plans.Add(this.floors[i].GetPlanImage2(_scale));

                

            }

            for (int i = 0; i < this.floors.Count; i++)
            {
                if (!this.floors[i].IsAllRoomsHasMinSquare(squareLimit))
                {
                    MessageBox.Show(
                        "Не удается сгенерировать заданное количество помещений с площадью не менее " + squareLimit +
                        "% от площади здания", "Ошибка [Этаж "+i.ToString()+"]", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                }

                if (!this.floors[i].IsAllRoomsHaveNotNarrowPart(narrowPartLimit))
                {
                    MessageBox.Show("Некоторые комнаты имеют узкие части", "Предупреждение [Этаж " + i.ToString() + "]", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                }
            }




            schema.Attributes = this.schemaAttributes;


            return schema;
        }

       

    }
}
