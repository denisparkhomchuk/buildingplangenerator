﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace BuildingPlanGenerator
{
    public static class ParamGenerator
    {
        public static int GetRoomCount(string _id, int _buildingLength, int _buildingWidth)
        {
            string commonString = _id + _buildingLength.ToString() + _buildingWidth.ToString();
            string hashString = GetHashString(commonString);

            // принимаем лимиты для параметра: 3 - 8
            //определяем значение по первой цифре в хэше

            int initNum=int.Parse(hashString.Substring(0, 1), System.Globalization.NumberStyles.HexNumber);

            if (initNum<3)
            {
                return 5;
            }
            else
            {
                if (initNum>8)
                {
                    return 6;
                }
                else
                {
                    return initNum;
                }
            }
        }

        public static int GetFloorCount(string _id, int _buildingLength, int _buildingWidth)
        {
            string commonString = _id + _buildingLength.ToString() + _buildingWidth.ToString();
            string hashString = GetHashString(commonString);

            // принимаем лимиты для параметра: 1 - 10
            //определяем значение по второй цифре в хэше

            int initNum = int.Parse(hashString.Substring(2, 1), System.Globalization.NumberStyles.HexNumber);

            if (initNum < 1)
            {
                return 1;
            }
            else
            {
                if (initNum > 10)
                {
                    return 5;
                }
                else
                {
                    return initNum;
                }
            }
        }

        public static byte[] GetHash(string _inputString)
        {
            HashAlgorithm algorithm = MD5.Create();  //or use SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(_inputString));
        }

        public static string GetHashString(string _inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(_inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
