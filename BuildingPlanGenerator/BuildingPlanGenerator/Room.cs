﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildingPlanGenerator
{
    class Room
    {
        public string SpaceMarker { get; set; }

        public Color Color { get; set; }

        public string Name { get; set; }

        public int Square { get; set; }

        public bool HasDoorToCorridor { get; set; }

        public bool IsNorthGrowthForbidden { get; set; }
        public bool IsSouthGrowthForbidden { get; set; }
        public bool IsWestGrowthForbidden { get; set; }
        public bool IsEastGrowthForbidden { get; set; }
        public int limitationNorth { get; set; }
        public int limitationSouth { get; set; }
        public int limitationWest { get; set; }
        public int limitationEast { get; set; }


        public Room(string _spaceMarker, string _name)
        {
            this.SpaceMarker = _spaceMarker;

            this.Color = Color.FromArgb(Rand.Next(256), Rand.Next(256), Rand.Next(256));
            this.Name = _name;
            this.HasDoorToCorridor = false;
            this.IsNorthGrowthForbidden = false;
            this.IsSouthGrowthForbidden = false;
            this.IsWestGrowthForbidden = false;
            this.IsEastGrowthForbidden = false;
            this.limitationNorth = Int32.MaxValue;
            this.limitationSouth = -1;
            this.limitationWest = Int32.MaxValue;
            this.limitationEast = -1;
        }

        public PointF GetCenter()
        {
            return new PointF(this.limitationNorth + (this.limitationSouth - this.limitationNorth) / 2, this.limitationWest+(this.limitationEast - this.limitationWest) / 2);
        }

    }
}
