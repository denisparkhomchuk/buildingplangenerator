﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace BuildingPlanGenerator
{
    public partial class FormMain : Form
    {
        private Planner planner;
        private Schema schema;

        

        public FormMain()
        {
            InitializeComponent();
            this.tabControlPlans.Visible = false;
            this.linkLabelOpenFile.Visible = false;
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            planner = new Planner();

            

            this.tabControlPlans.Visible = false;
            this.linkLabelOpenFile.Visible = false;
            this.tabControlPlans.Controls.Clear();

            string id = textBoxSchemeId.Text;

            int buildingLength = 0;
            int buildingWidth = 0;

            int floorNum = 0;
            int roomNum = 0;

            if (this.IsBuildingSizeValid(textBoxBuildingLength.Text, textBoxBuildingWidth.Text))
            {
                
            
         buildingLength = Int32.Parse(textBoxBuildingLength.Text) * 10; // перевод в десятые метра
         buildingWidth = Int32.Parse(textBoxBuildingWidth.Text) * 10; // перевод в десятые метра


          floorNum = ParamGenerator.GetFloorCount(id, buildingLength, buildingWidth);
           roomNum = ParamGenerator.GetRoomCount(id, buildingLength, buildingWidth);



            textBoxFloorNum.Text = floorNum.ToString();
            textBoxRoomNum.Text = roomNum.ToString();


            Rand.Seed(ParamGenerator.GetHashString(id+ buildingLength.ToString()+ buildingWidth.ToString()));

            double scaleCoeff = 0.0;
            double scaleCoeffI = 150*3.779527559/buildingLength;
            double scaleCoeffJ = 100 * 3.779527559 / buildingWidth;

            if (scaleCoeffI < scaleCoeffJ)
            {
                scaleCoeff = scaleCoeffI;
            }
            else
            {
                scaleCoeff = scaleCoeffJ;
            }

           
                if (this.IsRoomNumValid(roomNum))
                {
                    SchemaAttributes schemaAttributes = new SchemaAttributes(roomNum, floorNum, buildingLength, buildingWidth);
                    this.planner = new Planner(schemaAttributes);
                    schema = planner.Plan(ref pictureBoxMap, ref toolStripStatusLabel1, scaleCoeff);

             


                  this.tabControlPlans.Size = new System.Drawing.Size(planner.floors[0].BitmapPlan.Width+20, planner.floors[0].BitmapPlan.Height+50);
                  for (int i = 0; i < planner.floors.Count; i++)
                  {
                      this.AddPlanPageTab(i, scaleCoeff);
                      
                  }
                  this.tabControlPlans.Visible = true;
                    this.linkLabelOpenFile.Visible = true;

                    Cursor.Current = Cursors.Default;
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("Неверно задано количество комнат. Площадь комнаты не может быть менее 5% площади здания.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Неверно заданы размеры здания. Максимальный размер здания: 150 x 100", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;

        }

        private bool IsRoomNumValid(int _roomNum)
        {
            if (_roomNum > 18)
            {
                return false;
            }
            return true;
        }

        private bool IsBuildingSizeValid(string _length, string _width)
        {
            int length;
            int width;

            if (int.TryParse(_length, out length) && int.TryParse(_width, out width))
            {
                length = int.Parse(_length);
                width = int.Parse(_width);
                if (length>0 && width>0 && length<=150 && width <=100)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
       

       
        

        private void toolStripStatusLabel1_TextChanged(object sender, EventArgs e)
        {
            this.Refresh();
        }



        private void AddPlanPageTab(int _index, double _scaleCoeff)
        {



            this.tabControlPlans.Controls.Add(new TabPage());

            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count-1].Location = new System.Drawing.Point(4, 22);
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Name = "tabPage" + _index.ToString();
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Padding = new System.Windows.Forms.Padding(3);
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Size = new System.Drawing.Size(planner.floors[_index].BitmapPlan.Width, planner.floors[_index].BitmapPlan.Height);
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].TabIndex = _index;
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Text = _index.ToString();

            PictureBox pictureBox = new System.Windows.Forms.PictureBox();
            pictureBox.Image = planner.floors[_index].BitmapPlan;
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Controls.Add(pictureBox);
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Controls[this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Controls.Count - 1].Location = new System.Drawing.Point(5, 5);
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Controls[
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Controls.Count - 1].Name = "pictureBox";
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Controls[
            this.tabControlPlans.Controls[this.tabControlPlans.Controls.Count - 1].Controls.Count - 1].BackColor=Color.White;


            this.tabControlPlans.Controls[_index].Controls[0].Size = new System.Drawing.Size(planner.floors[_index].BitmapPlan.Width, planner.floors[_index].BitmapPlan.Height);

            

            
        }

        private void linkLabelOpenFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            
            string tempPlansFolder = System.IO.Directory.GetCurrentDirectory() + "\\temp_plans\\";

            string tempFileName = tempPlansFolder + Guid.NewGuid().ToString() + ".bmp";
            schema.Plans[tabControlPlans.SelectedIndex].Save(tempFileName);
            System.Diagnostics.Process.Start(tempFileName);
        }
     
    }
}
