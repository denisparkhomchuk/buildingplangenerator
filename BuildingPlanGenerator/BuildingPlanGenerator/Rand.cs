﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildingPlanGenerator
{
    class Rand
    {
        private static Random rand;

        //static Rand()
        //{
        //    rand = new Random(DateTime.Now.Millisecond);
        //}

        public static void Seed(string _hashString)
        {
            rand = new Random(int.Parse(_hashString.Substring(0,7), System.Globalization.NumberStyles.HexNumber));   
        }

        public static int Next(int _min, int _max)
        {
            return rand.Next(_min, _max);
        }

        public static int Next(int _max)
        {
            return rand.Next(_max);
        }
    }
}
