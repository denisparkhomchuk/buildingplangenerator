﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildingPlanGenerator
{
    class Schema
    {
        public List<Bitmap> Plans { get; set; }
        public SchemaAttributes Attributes { get; set; }

        public Schema()
        {
            Plans = new List<Bitmap>();
            Attributes = new SchemaAttributes();
        }
    }
}
