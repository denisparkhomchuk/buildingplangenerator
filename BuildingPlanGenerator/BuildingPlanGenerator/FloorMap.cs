﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuildingPlanGenerator
{

    class FloorMap
    {
        public FloorMapCell[,] Map { get; set; }
        public List<Room> rooms;
        public int Length { get; set; }
        public int Width { get; set; }

        public Bitmap BitmapPlan { get; set; }
        public int Square
        {
            get { return this.Length * this.Width; }
        }



        public FloorMap( SchemaAttributes _schemaAttributes)
        {
          

            this.Length = _schemaAttributes.BuildingLength;
            this.Width = _schemaAttributes.BuildingWidth;
            this.Map = new FloorMapCell[this.Length, this.Width];
            for (int i = 0; i < this.Length; i++)
            {
                for (int j = 0; j < this.Width; j++)
                {
                    this.Map[i, j] = new FloorMapCell();
                }
            }
            this.rooms = new List<Room>();
            for (int i = 0; i < _schemaAttributes.RoomNum; i++)
            {
                this.rooms.Add(new Room("Room" + i.ToString(), "Каб. " + i.ToString()));
            }

           
            
        }

        private FloorMapCell GetFloorMapCell(int _i, int _j)
        {
            if (_i<this.Length && _j<this.Width)
            {
                return this.Map[_i, _j];
            }
            return null;
        }
        public void MakeCorridors(ref PictureBox _pictureBox)
        {
            // Вычисление минимальных ширин коридоров
            int minWidthVerticalCorridor = 0;
            int minWidthHorisontalCorridor = 0;
            if ((int)(0.05 * this.Width) < 20)
            {
                minWidthVerticalCorridor = 20;
            }
            else
            {
                minWidthVerticalCorridor = (int)(0.05 * this.Width);
            }
            if ((int)(0.05 * this.Length) < 20)
            {
                minWidthHorisontalCorridor = 20;
            }
            else
            {
                minWidthHorisontalCorridor = (int)(0.05 * this.Length);
            }

           

            #region VerticalCorridor
            // делаем вертикальный корридор
            int currentI = 1;
            //устанавливаем, что корридор не быдет более чем на 20% от ширины приближатся к стенам здания
            int currentJ = Rand.Next((int)(1 + 0.2 * this.Width), (int)(this.Width - 1 - minWidthVerticalCorridor - (1 + 0.2 * this.Width)));

            int increment = 0;
            bool corridorFinished = false;

            List<Point> corridorStartCoordinates=new List<Point>();

            while (!corridorFinished)
            {


                increment = Rand.Next(currentI+ minWidthHorisontalCorridor, this.Length);

                if (this.Length - 1- increment < minWidthHorisontalCorridor)
                {
                    increment = this.Length - 2;
                }
                else
                {
                    do
                    {
                        currentJ = Rand.Next((int)(1 + 0.2 * this.Width), (int)(this.Width - 1 - minWidthVerticalCorridor - (1 + 0.2 * this.Width)));
                    } while (!this.IsCorridorFit(currentI, currentJ, minWidthHorisontalCorridor, minWidthVerticalCorridor));
                    
                }

                this.MarkCorridor(currentI, currentJ, increment, currentJ + minWidthVerticalCorridor);
                corridorStartCoordinates.Add(new Point(currentI,currentJ));
                corridorStartCoordinates.Add(new Point(increment, currentJ + minWidthVerticalCorridor));

                currentI = increment;
                if (currentI == this.Length - 2)
                {
                    corridorFinished = true;
                }
                else
                {
                    corridorFinished = false;
                }

                currentI = currentI - minWidthHorisontalCorridor;

                _pictureBox.Image = this.GetBitmap();
                _pictureBox.Refresh();
               

               
            }

            for (int i = 2; i < corridorStartCoordinates.Count-2; i++)
            {
                this.MarkCorridor(corridorStartCoordinates[i].X, corridorStartCoordinates[i].Y, corridorStartCoordinates[i-1].X, corridorStartCoordinates[i-1].Y);
                _pictureBox.Image = this.GetBitmap();
                _pictureBox.Refresh();
            }

            #endregion

        }

        public void MakeBuildingWalls(ref PictureBox _pictureBox)
        {
            for (int i = 0; i < this.Length; i++)
            {
                this.Map[i,0]=new FloorMapCell("BuildingWall",Color.Black,CellTypes.BuildingWall);
            }
            for (int i = 0; i < this.Length; i++)
            {
                this.Map[i, this.Width-1] = new FloorMapCell("BuildingWall", Color.Black, CellTypes.BuildingWall);
            }
            for (int j = 0; j < this.Width; j++)
            {
                this.Map[0, j] = new FloorMapCell("BuildingWall", Color.Black, CellTypes.BuildingWall);
            }
            for (int j = 0; j < this.Width; j++)
            {
                this.Map[this.Length-1, j] = new FloorMapCell("BuildingWall", Color.Black, CellTypes.BuildingWall);
            }

            _pictureBox.Image = this.GetBitmap();
            _pictureBox.Refresh();
        }


        public void MakeRooms(ref PictureBox _pictureBox)
        {
            this.SetInitRoomMarkers();

            int currentLimitationNorth;
            int currentLimitationSouth;
            int currentLimitationWest;
            int currentLimitationEast;
            while (!this.IsAllRoomGrowthForbidden())
            {



                for (int k = 0; k < this.rooms.Count; k++)
                {

                    #region North
                    //North
                    if (this.rooms[k].limitationNorth - 1 >= 1 && !this.rooms[k].IsNorthGrowthForbidden)
                    {
                        for (int j = 0; j < this.Width; j++)
                        {
                            if (this.GetFloorMapCell(this.rooms[k].limitationNorth, j).Marker == this.rooms[k].SpaceMarker &&
                                this.GetFloorMapCell(this.rooms[k].limitationNorth - 1, j).Marker != "Empty")
                            {
                                this.rooms[k].IsNorthGrowthForbidden = true;
                            }
                        }

                        if (!this.rooms[k].IsNorthGrowthForbidden)
                        {
                            currentLimitationNorth = this.rooms[k].limitationNorth;
                            for (int j = 0; j < this.Width; j++)
                            {
                                if (this.GetFloorMapCell(currentLimitationNorth, j).Marker ==
                                    this.rooms[k].SpaceMarker)
                                {
                                    this.MarkMapCellRoom(currentLimitationNorth - 1, j, k);

                                }
                            }
                        }
                    }
                    else
                    {
                        if (this.rooms[k].limitationNorth - 1 < 1)
                        {
                            this.rooms[k].IsNorthGrowthForbidden = true;
                        }
                    }
                    #endregion
                    #region West
                    //West
                    if (this.rooms[k].limitationWest - 1 >= 1 && !this.rooms[k].IsWestGrowthForbidden)
                    {
                        for (int i = 0; i < this.Length; i++)
                        {
                            if (this.GetFloorMapCell(i, this.rooms[k].limitationWest).Marker == this.rooms[k].SpaceMarker &&
                                this.GetFloorMapCell(i, this.rooms[k].limitationWest - 1).Marker != "Empty")
                            {
                                this.rooms[k].IsWestGrowthForbidden = true;
                            }
                        }
                        if (!this.rooms[k].IsWestGrowthForbidden)
                        {
                            currentLimitationWest = this.rooms[k].limitationWest;
                            for (int i = 0; i < this.Length; i++)
                            {
                                if (this.GetFloorMapCell(i, currentLimitationWest).Marker == this.rooms[k].SpaceMarker)
                                {
                                    this.MarkMapCellRoom(i, currentLimitationWest - 1, k);

                                }
                            }
                        }
                    }
                    else
                    {
                        if (this.rooms[k].limitationWest - 1 < 1)
                        {
                            this.rooms[k].IsWestGrowthForbidden = true;
                        }
                    }
                    #endregion
                    #region South
                    //South
                    if (this.rooms[k].limitationSouth + 1 < this.Length-1 && !this.rooms[k].IsSouthGrowthForbidden)
                    {


                        for (int j = 0; j < this.Width; j++)
                        {
                            if (this.GetFloorMapCell(this.rooms[k].limitationSouth, j).Marker == this.rooms[k].SpaceMarker &&
                                this.GetFloorMapCell(this.rooms[k].limitationSouth + 1, j).Marker != "Empty")
                            {
                                this.rooms[k].IsSouthGrowthForbidden = true;
                            }
                        }
                        if (!this.rooms[k].IsSouthGrowthForbidden)
                        {
                            currentLimitationSouth = this.rooms[k].limitationSouth;
                            for (int j = 0; j < this.Width; j++)
                            {
                                if (this.GetFloorMapCell(currentLimitationSouth, j).Marker ==
                                    this.rooms[k].SpaceMarker)
                                {
                                    this.MarkMapCellRoom(currentLimitationSouth + 1, j, k);

                                }
                            }
                        }
                    }
                    else
                    {
                        if (this.rooms[k].limitationSouth + 1 == this.Length-1)
                        {
                            this.rooms[k].IsSouthGrowthForbidden = true;
                        }
                    }
                    #endregion
                    #region East
                    //East
                    if (this.rooms[k].limitationEast + 1 < this.Width-1 && !this.rooms[k].IsEastGrowthForbidden)
                    {
                        for (int i = 0; i < this.Length; i++)
                        {
                            if (this.GetFloorMapCell(i, this.rooms[k].limitationEast).Marker == this.rooms[k].SpaceMarker &&
                                this.GetFloorMapCell(i, this.rooms[k].limitationEast + 1).Marker != "Empty")
                            {
                                this.rooms[k].IsEastGrowthForbidden = true;
                            }
                        }
                        if (!this.rooms[k].IsEastGrowthForbidden)
                        {
                            currentLimitationEast = this.rooms[k].limitationEast;
                            for (int i = 0; i < this.Length; i++)
                            {
                                if (this.GetFloorMapCell(i, currentLimitationEast).Marker == this.rooms[k].SpaceMarker)
                                {
                                    this.MarkMapCellRoom(i, currentLimitationEast + 1, k);

                                }
                            }
                        }

                    }
                    else
                    {
                        if (this.rooms[k].limitationEast + 1 == this.Width-1)
                        {
                            this.rooms[k].IsEastGrowthForbidden = true;
                        }
                    }
                    #endregion
                }

            }

            // Заполнение пустых оставшихся регионов
            while (this.HasEmptyCell())
            {

            #region West > East
                // Заполнение пустых оставшихся регионов West > East


            for (int i = 0; i < this.Length-1; i++)
            {
                for (int j = 0; j < this.Width - 2; j++)
                {
                    if (this.GetFloorMapCell(i, j).Marker != "Empty" && this.GetFloorMapCell(i, j + 1).Marker == "Empty")
                    {
                        if(this.GetFloorMapCell(i,j).Type==CellTypes.RoomInnerSpace)
                        {
                            this.MarkMapCellRoom(i, j + 1, this.GetRoomIndexByMarker(this.GetFloorMapCell(i, j).Marker));
                        }
                        

                        //_pictureBox.Image = this.GetBitmap();
                        //_pictureBox.Refresh();
                    }
                }
            }
                #endregion

            #region East > West
            if (this.HasEmptyCell())
                {
                    
            // Заполнение пустых оставшихся регионов East>West  


            for (int i = 0; i < this.Length-1; i++)
            {
                for (int j = this.Width - 1; j >1 ; j--)
                {
                    if (this.GetFloorMapCell(i, j).Marker != "Empty" && this.GetFloorMapCell(i, j - 1).Marker == "Empty")
                    {
                        if (this.GetFloorMapCell(i, j).Type == CellTypes.RoomInnerSpace)
                        {
                            this.MarkMapCellRoom(i, j - 1, this.GetRoomIndexByMarker(this.GetFloorMapCell(i, j).Marker));
                        }


                        //_pictureBox.Image = this.GetBitmap();
                        //_pictureBox.Refresh();
                    }
                }
            }
                }

            #endregion

            #region North > South
            if (this.HasEmptyCell())
                {
                    
              
            // Заполнение пустых оставшихся регионов North > South


            for (int i = 0; i < this.Length-2; i++)
            {
                for (int j = 0; j < this.Width-1; j++)
                {
                    if (this.GetFloorMapCell(i, j).Marker != "Empty" && this.GetFloorMapCell(i+1, j).Marker == "Empty")
                    {
                        if (this.GetFloorMapCell(i, j).Type == CellTypes.RoomInnerSpace)
                        {
                            this.MarkMapCellRoom(i+1,j, this.GetRoomIndexByMarker(this.GetFloorMapCell(i, j).Marker));
                        }


                        
                    }
                }
                //_pictureBox.Image = this.GetBitmap();
                //_pictureBox.Refresh();
            }
                }

            #endregion

            #region South > North

            if (this.HasEmptyCell())
                {
                    
               
            // Заполнение пустых оставшихся регионов South > North


            for (int i = this.Length - 1; i >1 ; i--)
            {
                for (int j = 0; j < this.Width-1; j++)
                {
                    if (this.GetFloorMapCell(i, j).Marker != "Empty" && this.GetFloorMapCell(i - 1, j).Marker == "Empty")
                    {
                        if (this.GetFloorMapCell(i, j).Type == CellTypes.RoomInnerSpace)
                        {
                            this.MarkMapCellRoom(i - 1, j, this.GetRoomIndexByMarker(this.GetFloorMapCell(i, j).Marker));
                        }


                        
                    }
                }
                //_pictureBox.Image = this.GetBitmap();
                //_pictureBox.Refresh();
            }
                }

            #endregion
            }
            

            _pictureBox.Image = this.GetBitmap();
            _pictureBox.Refresh();
        }

        private int GetRoomIndexByMarker(string _marker)
        {
            for (int i = 0; i < this.rooms.Count; i++)
            {
                if (this.rooms[i].SpaceMarker == _marker)
                {
                    return i;
                }
            }

            return -1;
        }

        private void MarkMapCellRoom(int _i, int _j, int _roomIndex)
        {
            this.Map[_i, _j].Marker = this.rooms[_roomIndex].SpaceMarker;
            this.Map[_i, _j].Color = this.rooms[_roomIndex].Color;
            this.Map[_i, _j].Type = CellTypes.RoomInnerSpace;
            // change room limitations
            if (_i < this.rooms[_roomIndex].limitationNorth)
            {
                this.rooms[_roomIndex].limitationNorth = _i;
            }
            if (_i > this.rooms[_roomIndex].limitationSouth)
            {
                this.rooms[_roomIndex].limitationSouth = _i;
            }
            if (_j < this.rooms[_roomIndex].limitationWest)
            {
                this.rooms[_roomIndex].limitationWest = _j;
            }
            if (_j > this.rooms[_roomIndex].limitationEast)
            {
                this.rooms[_roomIndex].limitationEast = _j;
            }

        }

        private bool IsAllRoomGrowthForbidden()
        {
            foreach (var room in this.rooms)
            {
                if (!room.IsEastGrowthForbidden || !room.IsNorthGrowthForbidden || !room.IsSouthGrowthForbidden || !room.IsWestGrowthForbidden)
                {
                    return false;
                }
            }
            return true;
        }

        private void SetInitRoomMarkers()
        {


            int i = 0;
            int j = 0;
            int corridorWestLimitation = this.GetCorridorWestLimitation();
            int corridorEastLimitation = this.GetCorridorEastLimitation();

            for (int k = 0; k < this.rooms.Count/2; k++)
            {
                do
                {
                    i = Rand.Next(this.Length);
                    j = Rand.Next(corridorWestLimitation);
                } while (this.GetFloorMapCell(i,j).Marker!="Empty");
                
                this.MarkMapCellRoom(i, j, k);
            }
            for (int k = this.rooms.Count / 2; k < this.rooms.Count ; k++)
            {
                do
                {
                    i = Rand.Next(this.Length);
                    j = Rand.Next(corridorEastLimitation, this.Width);
                } while (this.GetFloorMapCell(i, j).Marker != "Empty");

                this.MarkMapCellRoom(i, j, k);
            }



        }

        private int GetCorridorWestLimitation()
        {
            int limitation = this.Length - 1;
            for (int i = 0; i < this.Length; i++)
            {
                for (int j = 0; j < this.Width; j++)
                {
                    if (this.GetFloorMapCell(i, j).Type == CellTypes.Corridor)
                    {
                        if (j<limitation)
                        {
                            limitation = j;
                        }
                    }
                }
            }

            return limitation;
        }
        private int GetCorridorEastLimitation()
        {
            int limitation =0;
            for (int i = 0; i < this.Length; i++)
            {
                for (int j = 0; j < this.Width; j++)
                {
                    if (this.GetFloorMapCell(i, j).Type == CellTypes.Corridor)
                    {
                        if (j > limitation)
                        {
                            limitation = j;
                        }
                    }
                }
            }

            return limitation;
        }

        private bool IsCorridorFit(int _i, int _j, int _minWidthHor, int _minWidthVert)
        {
            if (_i + _minWidthHor -1 >= this.Length-1 || _j + _minWidthVert -1 >= this.Width-1)
            {
                return false;
            }
            for (int i = _i; i < _i + _minWidthHor; i++)
            {
                for (int j = _j; j < _j + _minWidthVert; j++)
                {
                    if (this.GetFloorMapCell(_i,_j).Type!=CellTypes.Empty)
                    {
                        return false;
                    } 
                }  
            }
            return true;
        }

        private void MarkCorridor(int _iStart, int _jStart, int _iEnd, int _jEnd)
        {
            if (_iStart<_iEnd)
            {
                if (_jStart < _jEnd)
                {
                    for (int i = _iStart; i <= _iEnd; i++)
                    {
                        for (int j = _jStart; j <= _jEnd; j++)
                        {
                            this.Map[i, j] = new FloorMapCell("Corridor", Color.Gray, CellTypes.Corridor);
                        }
                    }
                }
                else
                {
                    for (int i = _iStart; i <= _iEnd; i++)
                    {
                        for (int j = _jStart; j >= _jEnd; j--)
                        {
                            this.Map[i, j] = new FloorMapCell("Corridor", Color.Gray, CellTypes.Corridor);
                        }
                    } 
                }
            }
            else
            {
                if (_jStart < _jEnd)
                {
                    for (int i = _iStart; i >= _iEnd; i--)
                    {
                        for (int j = _jStart; j <= _jEnd; j++)
                        {
                            this.Map[i, j] = new FloorMapCell("Corridor", Color.Gray, CellTypes.Corridor);
                        }
                    }
                }
                else
                {
                    for (int i = _iStart; i >= _iEnd; i--)
                    {
                        for (int j = _jStart; j >= _jEnd; j--)
                        {
                            this.Map[i, j] = new FloorMapCell("Corridor", Color.Gray, CellTypes.Corridor);
                        }
                    }
                } 
            }

            
        }

        private bool HasEmptyCell()
        {
            for (int i = 0; i < this.Length; i++)
            {
                for (int j = 0; j < this.Width; j++)
                {
                    if (this.Map[i, j].Marker == "Empty")
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public Bitmap GetBitmap()
        {
            Bitmap bmp = new Bitmap(this.Length, this.Width);
            for (int i = 0; i < this.Length; i++)
            {
                for (int j = 0; j < this.Width; j++)
                {
                    bmp.SetPixel(i, j, this.GetFloorMapCell(i, j).Color);
                }
            }
            return bmp;
        }

        public void MakeRoomWalls(ref PictureBox _pictureBox)
        {
            for (int i = 0; i < this.Length-1; i++)
            {
                for (int j = 0; j < this.Width-1; j++)
                {
                    if (this.GetFloorMapCell(i,j).Type==CellTypes.RoomInnerSpace)
                    {
                        if ((this.GetFloorMapCell(i + 1, j).Type == CellTypes.RoomInnerSpace && this.GetFloorMapCell(i, j).Marker != this.GetFloorMapCell(i + 1, j).Marker) || (this.GetFloorMapCell(i + 1, j).Type == CellTypes.Corridor))
                        {
                           this.MarkRoomWall(i,j); 
                        }

                        if ((this.GetFloorMapCell(i, j + 1).Type == CellTypes.RoomInnerSpace && this.GetFloorMapCell(i, j).Marker != this.GetFloorMapCell(i, j + 1).Marker) || (this.GetFloorMapCell(i , j+1).Type == CellTypes.Corridor))
                        {
                            this.MarkRoomWall(i, j);
                        }

                        if ((this.GetFloorMapCell(i+1, j + 1).Type == CellTypes.RoomInnerSpace && this.GetFloorMapCell(i, j).Marker != this.GetFloorMapCell(i+1, j + 1).Marker) || (this.GetFloorMapCell(i+1, j + 1).Type == CellTypes.Corridor))
                        {
                            this.MarkRoomWall(i, j);
                        }
                    }
                }


            }

            for (int i = this.Length - 1; i >0 ; i--)
            {
                for (int j = this.Width - 1; j >0 ; j--)
                {
                    if (this.GetFloorMapCell(i, j).Type == CellTypes.RoomInnerSpace)
                    {
                        if ((this.GetFloorMapCell(i - 1, j).Type == CellTypes.RoomInnerSpace && this.GetFloorMapCell(i, j).Marker != this.GetFloorMapCell(i - 1, j).Marker) || (this.GetFloorMapCell(i - 1, j).Type == CellTypes.Corridor))
                        {
                            this.MarkRoomWall(i, j);
                        }

                        if ((this.GetFloorMapCell(i, j - 1).Type == CellTypes.RoomInnerSpace && this.GetFloorMapCell(i, j).Marker != this.GetFloorMapCell(i, j - 1).Marker) || (this.GetFloorMapCell(i, j - 1).Type == CellTypes.Corridor))
                        {
                            this.MarkRoomWall(i, j);
                        }

                        if ((this.GetFloorMapCell(i-1, j - 1).Type == CellTypes.RoomInnerSpace && this.GetFloorMapCell(i, j).Marker != this.GetFloorMapCell(i-1, j - 1).Marker) || (this.GetFloorMapCell(i-1, j - 1).Type == CellTypes.Corridor))
                        {
                            this.MarkRoomWall(i, j);
                        }
                    }
                }


            }

            for (int i = 0; i < this.Length - 1; i++)
            {
                for (int j = this.Width - 1; j > 0; j--)
                {
                    if (this.GetFloorMapCell(i, j).Type == CellTypes.RoomInnerSpace)
                    {
                        if ((this.GetFloorMapCell(i + 1, j-1).Type == CellTypes.RoomInnerSpace &&
                             this.GetFloorMapCell(i, j).Marker != this.GetFloorMapCell(i + 1, j-1).Marker) ||
                            (this.GetFloorMapCell(i + 1, j-1).Type == CellTypes.Corridor))
                        {
                            this.MarkRoomWall(i, j);
                        }
                    }
                }
            }

            for (int i = this.Length - 1; i >0 ; i--)
            {
                for (int j = 0; j < this.Width - 1; j++)
                {
                    if (this.GetFloorMapCell(i, j).Type == CellTypes.RoomInnerSpace)
                    {
                        if ((this.GetFloorMapCell(i - 1, j + 1).Type == CellTypes.RoomInnerSpace &&
                             this.GetFloorMapCell(i, j).Marker != this.GetFloorMapCell(i - 1, j + 1).Marker) ||
                            (this.GetFloorMapCell(i - 1, j + 1).Type == CellTypes.Corridor))
                        {
                            this.MarkRoomWall(i, j);
                        }
                    }
                }
            }

            _pictureBox.Image = this.GetBitmap();
            _pictureBox.Refresh();


        }

        private void MarkRoomWall(int _i, int _j)
        {
            this.Map[_i, _j].Marker = "RoomWall";
            this.Map[_i, _j].Color = Color.Aqua;
            this.Map[_i, _j].Type = CellTypes.RoomWall;

        }

        public void MakeDoors(ref PictureBox _pictureBox)
        {
            List<Point> cells=new List<Point>();
            Point insertionCell=new Point();
            int randInsertionParam = Rand.Next(4);

            for (int i = 0; i < this.rooms.Count; i++)
            {
                this.CalculateRoomLimitations(i);
            }

            while (!this.IsAllRoomsHaveDoor())
            {
                for (int i = 0; i < this.rooms.Count; i++)
                {
                    if (!this.rooms[i].HasDoorToCorridor)
                    {
                        randInsertionParam = Rand.Next(4);

                        switch (randInsertionParam)
                        {
                            case 0:
                                cells = this.GetDoorInsertionCells(i, "South");
                                if (cells.Count()>0)
                                {
                                    insertionCell = cells[Rand.Next(cells.Count)];
                                    this.InsertDoor(insertionCell.X, insertionCell.Y, "Horisontal");
                                    this.rooms[i].HasDoorToCorridor = true;  
                                }
                                
                                break;
                            case 1:
                                cells = this.GetDoorInsertionCells(i, "North");
                                if (cells.Count() > 0)
                                {
                                    insertionCell = cells[Rand.Next(cells.Count)];
                                    this.InsertDoor(insertionCell.X, insertionCell.Y, "Horisontal");
                                    this.rooms[i].HasDoorToCorridor = true;
                                }
                                break;

                            case 2:
                                cells = this.GetDoorInsertionCells(i, "West");
                                if (cells.Count() > 0)
                                {
                                    insertionCell = cells[Rand.Next(cells.Count)];
                                    this.InsertDoor(insertionCell.X, insertionCell.Y, "Vertical");
                                    this.rooms[i].HasDoorToCorridor = true;
                                }
                                break;
                            case 3:
                                cells = this.GetDoorInsertionCells(i, "East");
                                if (cells.Count() > 0)
                                {
                                    insertionCell = cells[Rand.Next(cells.Count)];
                                    this.InsertDoor(insertionCell.X, insertionCell.Y, "Vertical");
                                    this.rooms[i].HasDoorToCorridor = true;
                                }
                                break;
                        }
                    }

                  // _pictureBox.Image = this.GetBitmap();
                   // _pictureBox.Refresh();

                }

                //_pictureBox.Image = this.GetBitmap();
                //_pictureBox.Refresh();
            }
            
        }

        private bool IsAllRoomsHaveDoor()
        {
            foreach (var room in this.rooms)
            {
                if (!room.HasDoorToCorridor)
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Метод для получения начальных ячеек вставки двери. Считаем что ширина двери - 10 ячеек
        /// </summary>
        /// <param name="_roomIndex">индекс комнаты</param>
        /// <param name="_direction">направление вставки: горизонтальное, вертикальное</param>
        /// <returns></returns>
        private List<Point> GetDoorInsertionCells(int _roomIndex, string _direction)
        {
           List<Point> cells=new List<Point>();

           for (int i = this.rooms[_roomIndex].limitationNorth; i < this.rooms[_roomIndex].limitationSouth+1; i++)
            {
                for (int j = this.rooms[_roomIndex].limitationWest; j < this.rooms[_roomIndex].limitationEast+1; j++)
                {
                    switch (_direction)
                    {
                        case "South":
                            if (this.IsDoorFit(_roomIndex,"Horisontal",i,j,"South"))
                            {
                                cells.Add(new Point(i+1,j));
                            }
                            break;
                        case "North":
                            if (this.IsDoorFit(_roomIndex, "Horisontal", i, j, "North"))
                            {
                                cells.Add(new Point(i - 1, j));
                            }
                            break;
                        case "West":
                            if (this.IsDoorFit(_roomIndex, "Vertical", i, j, "West"))
                            {
                                cells.Add(new Point(i, j-1));
                            }
                            break;
                        case "East":
                            if (this.IsDoorFit(_roomIndex, "Vertical", i, j, "East"))
                            {
                                cells.Add(new Point(i, j + 1));
                            }
                            break;
                    }
                }
            }


            return cells;
        }

        private bool IsDoorFit(int _roomIndex, string _direction, int _i, int _j, string _corridorPlacement)
        {
            bool corridor = true;
            bool neighbourRoom = true;

            if (_direction=="Horisontal")
            {
                if (_corridorPlacement=="South")
                {
                    if (_j+10<=this.Width && _i+2<this.Length)
                    {
                        
                   

                    for (int j = _j; j < _j + 10; j++)
                    {
                        if (this.GetFloorMapCell(_i, j).Type != CellTypes.RoomInnerSpace || this.GetFloorMapCell(_i, j).Marker != this.rooms[_roomIndex].SpaceMarker || this.GetFloorMapCell(_i + 1, j).Type != CellTypes.RoomWall || this.GetFloorMapCell(_i + 2, j).Type != CellTypes.Corridor)
                        {
                            corridor = false;
                            
                        }
                    }



                    for (int j = _j; j < _j + 10; j++)
                    {
                        if (this.GetFloorMapCell(_i, j).Marker != this.rooms[_roomIndex].SpaceMarker || this.GetFloorMapCell(_i + 1, j).Type != CellTypes.RoomWall || this.GetFloorMapCell(_i + 2, j).Type != CellTypes.RoomInnerSpace || !this.rooms[this.GetRoomIndexByMarker(this.GetFloorMapCell(_i + 2, j).Marker)].HasDoorToCorridor)
                        {
                            neighbourRoom = false;

                        }
                    }
                    }
                    else
                    {
                        return false;
                    }

                    
                }
                if (_corridorPlacement == "North")
                {
                    if (_j + 10 <= this.Width && _i - 2 >=0)
                    {
                        for (int j = _j; j < _j + 10; j++)
                        {
                            if (this.GetFloorMapCell(_i, j).Type != CellTypes.RoomInnerSpace ||
                                this.GetFloorMapCell(_i, j).Marker != this.rooms[_roomIndex].SpaceMarker ||
                                this.GetFloorMapCell(_i - 1, j).Type != CellTypes.RoomWall ||
                                this.GetFloorMapCell(_i - 2, j).Type != CellTypes.Corridor)
                            {
                                corridor = false;
                            }
                        }

                        for (int j = _j; j < _j + 10; j++)
                        {
                            if (this.GetFloorMapCell(_i, j).Marker != this.rooms[_roomIndex].SpaceMarker ||
                                this.GetFloorMapCell(_i - 1, j).Type != CellTypes.RoomWall ||
                                this.GetFloorMapCell(_i - 2, j).Type != CellTypes.RoomInnerSpace ||
                                !this.rooms[this.GetRoomIndexByMarker(this.GetFloorMapCell(_i - 2, j).Marker)]
                                    .HasDoorToCorridor)
                            {
                                neighbourRoom = false;

                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            if (_direction == "Vertical")
            {
                if (_i+10<=this.Length && _j-2>=0)
                    {
                        if (_corridorPlacement == "West")
                        {
                            for (int i = _i; i < _i + 10; i++)
                            {
                                if (this.GetFloorMapCell(i, _j).Type != CellTypes.RoomInnerSpace ||
                                    this.GetFloorMapCell(i, _j).Marker != this.rooms[_roomIndex].SpaceMarker ||
                                    this.GetFloorMapCell(i, _j - 1).Type != CellTypes.RoomWall ||
                                    this.GetFloorMapCell(i, _j - 2).Type != CellTypes.Corridor)
                                {
                                    corridor = false;
                                }
                            }

                            for (int i = _i; i < _i + 10; i++)
                            {
                                if (this.GetFloorMapCell(i, _j).Marker != this.rooms[_roomIndex].SpaceMarker ||
                                    this.GetFloorMapCell(i, _j - 1).Type != CellTypes.RoomWall ||
                                    this.GetFloorMapCell(i, _j - 2).Type != CellTypes.RoomInnerSpace ||
                                    !this.rooms[this.GetRoomIndexByMarker(this.GetFloorMapCell(i, _j - 2).Marker)]
                                        .HasDoorToCorridor)
                                {
                                    neighbourRoom = false;
                                }
                            }
                        }

                    }
                else
                {
                    return false;
                }
                if (_corridorPlacement == "East")
                {
                    if (_i + 10 <= this.Length && _j + 2 < this.Width)
                    {
                        for (int i = _i; i < _i + 10; i++)
                        {
                            if (this.GetFloorMapCell(i, _j).Type != CellTypes.RoomInnerSpace ||
                                this.GetFloorMapCell(_i, _j).Marker != this.rooms[_roomIndex].SpaceMarker ||
                                this.GetFloorMapCell(i, _j + 1).Type != CellTypes.RoomWall ||
                                this.GetFloorMapCell(i, _j + 2).Type != CellTypes.Corridor)
                            {
                                corridor = false;
                            }
                        }
                        for (int i = _i; i < _i + 10; i++)
                        {
                            if (this.GetFloorMapCell(i, _j).Marker != this.rooms[_roomIndex].SpaceMarker ||
                                this.GetFloorMapCell(i, _j + 1).Type != CellTypes.RoomWall ||
                                this.GetFloorMapCell(i, _j + 2).Type != CellTypes.RoomInnerSpace ||
                                !this.rooms[this.GetRoomIndexByMarker(this.GetFloorMapCell(i, _j + 2).Marker)]
                                    .HasDoorToCorridor)
                            {
                                neighbourRoom = false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            
            if (!corridor && !neighbourRoom)
            {
                return false;
            }
            return true;
        }

        private void InsertDoor(int _i, int _j, string _direction)
        {
            switch (_direction)
            {
                case "Horisontal":
                    for (int j = _j; j < _j+10; j++)
                    {
                        this.Map[_i, j].Type = CellTypes.Door;
                        this.Map[_i, j].Color = Color.Crimson;
                        this.Map[_i, j].Marker = "Door";
                    }
                    break;
                case "Vertical":
                    for (int i = _i; i < _i + 10; i++)
                    {
                        this.Map[i, _j].Type = CellTypes.Door;
                        this.Map[i, _j].Color = Color.Crimson;
                        this.Map[i, _j].Marker = "Door";
                    }
                    break;
            }
        }

        public Bitmap GetPlanImage(int _scale)
        {
            Bitmap bmp = new Bitmap(this.Length, this.Width);
            Pen penWall = new Pen(Color.Black, 1);
            Pen penBuildingWall = new Pen(Color.Black, 1);
            Pen penRoomDoor = new Pen(Color.White, 1);
            
           
            
            Graphics formGraphics = Graphics.FromImage(bmp);
            for (int i = 0; i < this.Length; i++)
            {
                for (int j = 0; j < this.Width; j++)
                {
                    if (this.GetFloorMapCell(i,j).Type==CellTypes.RoomWall)
                    {
                        formGraphics.DrawRectangle(penWall, i, j, 1, 1);
                    }

                    if (this.GetFloorMapCell(i,j).Type==CellTypes.BuildingWall)
                    {
                        formGraphics.DrawRectangle(penBuildingWall, i, j, 1, 1);
                    }
                    if (this.GetFloorMapCell(i,j).Type==CellTypes.Door)
                    {
                        formGraphics.DrawRectangle(penRoomDoor, i, j, 1, 1);
                    }

                }
            }

            // отрисовка названий комнат
            int fontSize = 10;
            Size nameSize=new Size();
            Font roomNameFontHorisontal = new Font(FontFamily.GenericMonospace, fontSize, FontStyle.Regular, GraphicsUnit.Pixel);
            StringFormat verticalFormat = new StringFormat(StringFormatFlags.DirectionVertical);
           

            bool leave = false;
            bool nameInserted = false;


            

            for (int k = 0; k < this.rooms.Count; k++)
            {
                //попытка горизонтальной вставки
                leave = false;
                nameInserted = false;

                string roomSquare = ((double) (this.rooms[k].Square)/100).ToString();

                for (int i = 0; i < this.Length; i++)
                {
                    for (int j = 0; j < this.Width; j++)
                    {
                        nameSize = TextRenderer.MeasureText(this.rooms[k].Name + " (" + roomSquare + ")", roomNameFontHorisontal);
                        
                        if (this.IsCellFitToTextPlacement(i, j, k, nameSize.Height, nameSize.Width,"Horisontal"))
                        {
                            formGraphics.DrawString(this.rooms[k].Name + " (" + roomSquare + ")", roomNameFontHorisontal, Brushes.Black, i, j);
                           
                            leave = true;
                            nameInserted = true;
                            break;
                        }
                        
                    }
                    if (leave)
                    {
                        break;
                    }
                }

                //попытка вертикальной вставки
                if (!nameInserted)
                {
                    
                
                leave = false;
                nameInserted = false;
                for (int i = 0; i < this.Length; i++)
                {
                    for (int j = 0; j < this.Width; j++)
                    {
                        nameSize = TextRenderer.MeasureText(this.rooms[k].Name + " (" + roomSquare + ")", roomNameFontHorisontal);

                        if (this.IsCellFitToTextPlacement(i, j, k, nameSize.Height, nameSize.Width, "Vertical"))
                        {
                            formGraphics.DrawString(this.rooms[k].Name + " (" + roomSquare + ")", roomNameFontHorisontal, Brushes.Black, i, j, verticalFormat);
                            leave = true;
                            nameInserted = true;
                            break;
                        }

                    }
                    if (leave)
                    {
                        break;
                    }
                }
                }

            }




            return new Bitmap(bmp, (int)(this.Length * _scale), (int)(this.Width * _scale));

        }

        public Bitmap GetPlanImage2(double _scale)
        {
            

            #region отрисовка плана
            Bitmap bmp = new Bitmap(this.Length, this.Width);
            Pen penWall = new Pen(Color.Black, 1);
            Pen penBuildingWall = new Pen(Color.Black, 1);
            Pen penRoomDoor = new Pen(Color.White, 1);



            //for (int i = 0; i < bmp.Width; i++)
            //{
            //    for (int j = 0; j < bmp.Height; j++)
            //    {
            //        bmp.SetPixel(i,j,Color.White);
            //    }
            //}

            Graphics formGraphics = Graphics.FromImage(bmp);
            for (int i = 0; i < this.Length; i++)
            {
                for (int j = 0; j < this.Width; j++)
                {
                    if (this.GetFloorMapCell(i, j).Type == CellTypes.RoomWall)
                    {
                        formGraphics.DrawRectangle(penWall, i, j, 1, 1);
                    }

                    if (this.GetFloorMapCell(i, j).Type == CellTypes.BuildingWall)
                    {
                        formGraphics.DrawRectangle(penBuildingWall, i, j, 1, 1);
                    }
                    //if (this.GetFloorMapCell(i, j).Type == CellTypes.Door)
                    //{
                    //    formGraphics.DrawRectangle(penRoomDoor, i, j, 1, 1);
                    //}

                }
            }

            #endregion

            #region вычисление точек вставки названий комнат

            List<TextInsertionPoint> textInsertionPoints=new List<TextInsertionPoint>();

            int defaultFontSize = 0;
            if (_scale>=0.8 && _scale<1.5)
            {
                defaultFontSize = 16;
            }
            if (_scale >= 1.5 && _scale < 2.5)
            {
                defaultFontSize = 8;
            }
            if (_scale >= 2.5 && _scale < 3.5)
            {
                defaultFontSize = 6;
            }
            if (_scale >= 3.5 && _scale < 4.5)
            {
                defaultFontSize = 4;
            }
            if (_scale >= 4.5)
            {
                defaultFontSize = 3;
            }
            //
            if (_scale >= 0.6 && _scale < 0.8)
            {
                defaultFontSize = 18;
            }
            if (_scale >= 0.55 && _scale < 0.6)
            {
                defaultFontSize = 28;
            }
            if (_scale >= 0.3 && _scale < 0.55)
            {
                defaultFontSize = 40;
            }
            if (_scale >= 0.15 && _scale < 0.3)
            {
                defaultFontSize = 52;
            }
            if ( _scale < 0.15)
            {
                defaultFontSize = 72;
            }


            int fontSize = defaultFontSize;
            Size nameSize = new Size();
            System.Drawing.Text.PrivateFontCollection privateFonts = new System.Drawing.Text.PrivateFontCollection();
            privateFonts.AddFontFile("arial.ttf");
            Font roomNameFontHorisontal = new Font(privateFonts.Families[0], fontSize, FontStyle.Regular, GraphicsUnit.Pixel);
            StringFormat verticalFormat = new StringFormat(StringFormatFlags.DirectionVertical);


            bool leave = false;
            bool nameInserted = false;

            


            for (int k = 0; k < this.rooms.Count; k++)
            {
                fontSize = defaultFontSize;
                roomNameFontHorisontal = new Font(privateFonts.Families[0], fontSize, FontStyle.Regular, GraphicsUnit.Pixel);
                leave = false;
                nameInserted = false;

                string roomSquare = ((double)(this.rooms[k].Square) / 100).ToString();

                while (!nameInserted)
                {
                    //попытка горизонтальной вставки


                    // отступ 2 ячейки
                    for (int i = this.rooms[k].limitationNorth+2; i < this.rooms[k].limitationSouth; i++)
                    {
                        for (int j = this.rooms[k].limitationWest+2; j < this.rooms[k].limitationEast; j++)
                        {
                            nameSize = TextRenderer.MeasureText(this.rooms[k].Name + " (" + roomSquare + ")", roomNameFontHorisontal);

                            if (this.IsCellFitToTextPlacement(i, j, k, nameSize.Height, nameSize.Width, "Horisontal"))
                            {
                                //formGraphics.DrawString(this.rooms[k].Name + " (" + roomSquare + ")", roomNameFontHorisontal, Brushes.Black, i, j);
                                textInsertionPoints.Add(new TextInsertionPoint(new Point(i, j), "Horisontal", fontSize));

                                leave = true;
                                nameInserted = true;
                                break;
                            }

                        }
                        if (leave)
                        {
                            break;
                        }
                    }

                    //попытка вертикальной вставки
                    if (!nameInserted)
                    {


                        leave = false;
                        nameInserted = false;
                        for (int i = this.rooms[k].limitationNorth; i < this.rooms[k].limitationSouth; i++)
                        {
                            for (int j = this.rooms[k].limitationWest; j < this.rooms[k].limitationEast; j++)
                            {
                                nameSize = TextRenderer.MeasureText(this.rooms[k].Name + " (" + roomSquare + ")", roomNameFontHorisontal);

                                if (this.IsCellFitToTextPlacement(i, j, k, nameSize.Height, nameSize.Width, "Vertical"))
                                {

                                    textInsertionPoints.Add(new TextInsertionPoint(new Point(i, j), "Vertical", fontSize));
                                    leave = true;
                                    nameInserted = true;
                                    break;
                                }

                            }
                            if (leave)
                            {
                                break;
                            }
                        }
                    }

                    fontSize--;
                    roomNameFontHorisontal = new Font(privateFonts.Families[0], fontSize, FontStyle.Regular, GraphicsUnit.Pixel);
                }
                
               

            }

            #endregion


            Bitmap scaledBitmap = new Bitmap(bmp, (int)(this.Length * _scale), (int)(this.Width * _scale));
            

            //// Повышаем четкость чертежа
            //for (int i = 0; i < scaledBitmap.Width; i++)
            //{
            //    for (int j = 0; j < scaledBitmap.Height; j++)
            //    {
            //        if (scaledBitmap.GetPixel(i, j) != Color.White)
            //        {
            //           scaledBitmap.SetPixel(i,j,Color.Black); 
            //        }
            //    }
            //}

            formGraphics = Graphics.FromImage(scaledBitmap);


            int scaledFontSize = defaultFontSize;

            Font scaledRoomNameFontHorisontal = new Font(privateFonts.Families[0], scaledFontSize, FontStyle.Regular, GraphicsUnit.Pixel);

            for (int k = 0; k < this.rooms.Count; k++)
            {
                scaledFontSize = (int)(textInsertionPoints[k].FontSize*_scale);
                scaledRoomNameFontHorisontal = new Font(privateFonts.Families[0], scaledFontSize, FontStyle.Regular, GraphicsUnit.Pixel);

                string roomSquare = ((double)(this.rooms[k].Square) / 100).ToString();
                if (textInsertionPoints[k].Direction == "Horisontal")
                {
                    formGraphics.DrawString(this.rooms[k].Name + " (" + roomSquare + ")", scaledRoomNameFontHorisontal, Brushes.Black, (int)(textInsertionPoints[k].Point.X*_scale), (int)(textInsertionPoints[k].Point.Y*_scale));
                }
                else
                {
                    formGraphics.DrawString(this.rooms[k].Name + " (" + roomSquare + ")", scaledRoomNameFontHorisontal, Brushes.Black, (int)(textInsertionPoints[k].Point.X*_scale), (int)(textInsertionPoints[k].Point.Y*_scale), verticalFormat);
                }

            }


            this.BitmapPlan = scaledBitmap;

            return scaledBitmap;

        }

        public bool IsAllRoomsHasMinSquare(int _percent)
        {
            for (int i = 0; i < this.rooms.Count; i++)
            {
                this.CalculateRoomSquare(i);
                if (this.rooms[i].Square < this.Square * _percent / 100)
                {
                    return false;
                }
            }
            return true;
        }


        private void CalculateRoomLimitations(int _roomIndex)
        {
            int limitationNorth = Int32.MaxValue;
            int limitationSouth = -1;
            int limitationWest = Int32.MaxValue;
            int limitationEast = -1;

            for (int i = 0; i < this.Length; i++)
            {
                for (int j = 0; j < this.Width; j++)
                {
                    if (this.GetFloorMapCell(i, j).Marker == this.rooms[_roomIndex].SpaceMarker && i < limitationNorth)
                    {
                        limitationNorth = i;
                    }
                    if (this.GetFloorMapCell(i, j).Marker == this.rooms[_roomIndex].SpaceMarker && i > limitationSouth)
                    {
                        limitationSouth = i;
                    }
                    if (this.GetFloorMapCell(i, j).Marker == this.rooms[_roomIndex].SpaceMarker && j < limitationWest)
                    {
                        limitationWest = j;
                    }
                    if (this.GetFloorMapCell(i, j).Marker == this.rooms[_roomIndex].SpaceMarker && j > limitationEast)
                    {
                        limitationEast = j;
                    }

                }


            }

            this.rooms[_roomIndex].limitationNorth = limitationNorth;
            this.rooms[_roomIndex].limitationSouth = limitationSouth;
            this.rooms[_roomIndex].limitationWest = limitationWest;
            this.rooms[_roomIndex].limitationEast = limitationEast;
        }

        private void CalculateRoomSquare(int _roomIndex)
        {
            int square = 0;
            for (int i = 0; i < this.Length; i++)
            {
                for (int j = 0; j < this.Width; j++)
                {
                    if (this.GetFloorMapCell(i,j).Marker== this.rooms[_roomIndex].SpaceMarker)
                    {
                        square++;
                    }

                }
            }

            this.rooms[_roomIndex].Square = square;
        }

   

        private bool IsCellFitToTextPlacement(int _i, int _j, int _roomIndex, int _textHeight, int _textLength, string _direction)
        {
            switch (_direction)
            {
                case "Horisontal":
                    for (int i = _i; i < _i + _textLength; i++)
                    {
                        for (int j = _j; j < _j + _textHeight; j++)
                        {
                            if (this.GetFloorMapCell(i, j).Marker != this.rooms[_roomIndex].SpaceMarker)
                            {
                                return false;
                            }
                        }
                    }
                    return true;
                    break;
                case "Vertical":
                   for (int i = _i; i < _i + _textHeight; i++)
                    {
                        for (int j = _j; j < _j + _textLength; j++)
                        {
                            if (this.GetFloorMapCell(i, j).Marker != this.rooms[_roomIndex].SpaceMarker)
                            {
                                return false;
                            }
                        }
                    }
                    return true;
                    break;
            }

            
            return false;

        }

        public int GetEmptySquare()
        {
            int square = 0;
            for (int i = 0; i < this.Length; i++)
            {
                for (int j = 0; j < this.Width; j++)
                {
                    if (this.GetFloorMapCell(i, j).Type==CellTypes.Empty)
                    {
                        square++;
                    }

                }
            }

            return square;
        }

        private int CalculateRoomMinSize()
        {

            int minSize = Int32.MaxValue;
            int currentSize = 0;

            foreach (var room in this.rooms)
            {
                // вычисляем минимал. горизонт.  размер в комнате
                for (int i = room.limitationNorth; i <= room.limitationSouth; i++)
                {
                    currentSize = 0;
                    for (int j = room.limitationWest; j <= room.limitationEast; j++)
                    {
                        if (this.GetFloorMapCell(i, j).Marker==room.SpaceMarker)
                        {
                            currentSize++;
                        }
                        else
                        {
                            if (currentSize!=0)
                            {
                                minSize = Math.Min(minSize, currentSize);
                            }
                            
                            currentSize = 0;
                        }
                    }
                }
                // вычисляем минимал. горизонт.  размер в комнате

                for (int j = room.limitationWest; j <= room.limitationEast; j++)
                {
                    currentSize = 0;
                for (int i = room.limitationNorth; i <= room.limitationSouth; i++)
                {
                    
                    
                        if (this.GetFloorMapCell(i, j).Marker == room.SpaceMarker)
                        {
                            currentSize++;
                        }
                        else
                        {
                            if (currentSize != 0)
                            {
                                minSize = Math.Min(minSize, currentSize);
                            }

                            currentSize = 0;
                        }
                    }
                }
                ///////////////////
            }

            return minSize;
        }

        public bool IsAllRoomsHaveNotNarrowPart(int _limit)
        {
            if (this.CalculateRoomMinSize()>_limit)
            {
                return true;
            }
           return false;
        }
    }
}
