﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildingPlanGenerator
{
    class TextInsertionPoint
    {
        public Point Point { get; set; }
        public String Direction { get; set; }

        public int FontSize { get; set; }

        public TextInsertionPoint(Point _point, string _direction, int _fontSize)
        {
            this.Point = _point;
            this.Direction = _direction;
            this.FontSize = _fontSize;
        }

    }
}
