﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildingPlanGenerator
{
    enum CellTypes
    {
        Empty, //свободная площадь 
        RoomInnerSpace, // внутреннее пространство комнаты
        RoomWall, // стена комнаты
        BuildingWall, // стена здания
        Corridor, // корридор
        Door // дверь

    }
    class FloorMapCell
    {
        public Color Color { get; set; }
        public string Marker { get; set; }

        public CellTypes Type { get; set; }

        public FloorMapCell()
        {
            this.Marker = "Empty";
            this.Color = System.Drawing.Color.White;
            this.Type = CellTypes.Empty;
        }
        public FloorMapCell(string _marker, Color _color, CellTypes _type)
        {
            this.Marker = _marker;
            this.Color = _color;
            this.Type = _type;
        }

    }
}
