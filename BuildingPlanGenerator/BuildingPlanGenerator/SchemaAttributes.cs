﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildingPlanGenerator
{
    class SchemaAttributes
    {
        public int RoomNum { get; set; }
        public int FloorNum { get; set; }
        public int BuildingLength { get; set; }
        public int BuildingWidth { get; set; }

        public SchemaAttributes(int _roomNum, int _floorNum, int _buildLength, int _buildWidth)
        {
            this.RoomNum = _roomNum;
            this.FloorNum = _floorNum;
            this.BuildingLength = _buildLength;
            this.BuildingWidth = _buildWidth;
        }

        public SchemaAttributes()
        {
            this.RoomNum = 0;
            this.FloorNum = 0;
            this.BuildingLength = 0;
            this.BuildingWidth = 0;
        }
    }
}
